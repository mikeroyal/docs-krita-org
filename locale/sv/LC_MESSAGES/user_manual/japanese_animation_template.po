# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-07-31 13:12+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../user_manual/japanese_animation_template.rst:1
msgid "Detailed explanation on how to use the animation template."
msgstr "Detaljerad förklaring av hur animeringsmallarna används."

#: ../../user_manual/japanese_animation_template.rst:12
msgid "Animation"
msgstr "Animering"

#: ../../user_manual/japanese_animation_template.rst:12
msgid "Template"
msgstr "Mallar"

#: ../../user_manual/japanese_animation_template.rst:17
msgid "Japanese Animation Template"
msgstr "Japanska animeringsmallar"

#: ../../user_manual/japanese_animation_template.rst:20
msgid ""
"This template is used to make Japanese-style animation. It is designed on "
"the assumption that it was used in co-production, so please customize its "
"things like layer folders according to scale and details of your works."
msgstr ""
"Den här mallen används för att skapa animeringar i japansk stil. Den är "
"konstruerad med förutsättningen att den används i samproduktion, så anpassa "
"gärna saker som lagerkataloger enligt skalan och detaljrikedomen hos alstren."

#: ../../user_manual/japanese_animation_template.rst:26
msgid "Basic structure of its layers"
msgstr "Lagrens grundstruktur"

#: ../../user_manual/japanese_animation_template.rst:28
msgid ""
"Layers are organized so that your work will start from lower layers go to "
"higher layers, except for coloring layers."
msgstr ""
"Lager är organiserade så att arbetet från lägre lager till högre lager, utom "
"för färgläggningslager."

#: ../../user_manual/japanese_animation_template.rst:32
msgid ".. image:: images/animation/Layer_Organization.png"
msgstr ".. image:: images/animation/Layer_Organization.png"

#: ../../user_manual/japanese_animation_template.rst:34
msgid "Its layer contents"
msgstr "Dess lagerinnehåll"

#: ../../user_manual/japanese_animation_template.rst:36
msgid "from the bottom"
msgstr "nerifrån"

#: ../../user_manual/japanese_animation_template.rst:38
msgid "Layout Paper"
msgstr "Layoutpapper"

#: ../../user_manual/japanese_animation_template.rst:39
msgid ""
"These layers are a form of layout paper. Anime tap holes are prepared on "
"separate layers in case you have to print it out and continue your drawing "
"traditionally."
msgstr ""
"Lagren är en form av layoutpapper. Anime inriktningshål är förberedda på "
"separata lager ifall man måste skriva ut dem och fortsätta med traditionell "
"teckning."

#: ../../user_manual/japanese_animation_template.rst:40
msgid "Layout (Background)"
msgstr "Layout (bakgrund)"

#: ../../user_manual/japanese_animation_template.rst:41
msgid ""
"These layers will contain background scenery or layouts which are scanned "
"from a traditional drawing. If you don't use them, you can remove them."
msgstr ""
"Dessa lager innehåller bakgrundssceneri eller layouter som läses in från en "
"traditionell teckning. Om man inte använder dem, kan man ta bort dem."

#: ../../user_manual/japanese_animation_template.rst:42
msgid "Key drafts"
msgstr "Nyckelutkast"

#: ../../user_manual/japanese_animation_template.rst:43
msgid "These layers are used to draw layouts digitally."
msgstr "Dessa lager används för att teckna layouter digitalt."

#: ../../user_manual/japanese_animation_template.rst:44
msgid "Keys"
msgstr "Nyckelbilder"

#: ../../user_manual/japanese_animation_template.rst:45
msgid ""
"Where you add some details to the layouts and arrange them to draw \"keys\" "
"of animation."
msgstr ""
"Där man lägger till vissa detaljer i layouterna och arrangerar dem för att "
"rita animeringens \"nyckelbilder\"."

#: ../../user_manual/japanese_animation_template.rst:46
msgid "Inbetweening"
msgstr "Mellanteckning"

#: ../../user_manual/japanese_animation_template.rst:47
msgid ""
"Where you add inbetweens to keys for the process of coloring, and remove "
"unnecessary details to finalize keys (To be accurate, I finish finalization "
"of keys before beginning to add inbetweens)."
msgstr ""
"Där man lägger till mellanteckningar i nyckelbilder för "
"färgläggningsprocessen, och tar bort onödiga detaljer för att färdigställa "
"nyckelbilder (mer precist, avslutar jag nyckelbildernas färdigställande "
"innan jag börjar lägga till mellanteckningar)."

#: ../../user_manual/japanese_animation_template.rst:48
msgid "Coloring (under Inbetweening)"
msgstr "Färgläggning (under mellanteckning)"

#: ../../user_manual/japanese_animation_template.rst:49
msgid ""
"Where you fill areas with colors according to specification of inbetweens."
msgstr ""
"Där man fyller i områden med färger enligt mellanteckningens specifikation."

#: ../../user_manual/japanese_animation_template.rst:50
msgid "Time Sheet and Composition sheet"
msgstr "Tidsblad och kompositionsblad"

#: ../../user_manual/japanese_animation_template.rst:51
msgid ""
"This contains a time sheet and composition sheet. Please rotate them before "
"using."
msgstr ""
"Innehåller ett tidsblad och kompositionsblad. Rotera dem innan användning."

#: ../../user_manual/japanese_animation_template.rst:53
msgid "Color set"
msgstr "Färguppsättning"

#: ../../user_manual/japanese_animation_template.rst:53
msgid ""
"This contains colors used to draw main and auxiliary line art and fill "
"highlight or shadows. You can add them to your palette."
msgstr ""
"Innehåller färger använda för att rita huvud- och extrateckning och fylla i "
"dagrar eller skuggor. Man kan lägga till dem i paletten."

#: ../../user_manual/japanese_animation_template.rst:56
msgid "Basic steps to make animation"
msgstr "Grundsteg för att skapa animering"

#: ../../user_manual/japanese_animation_template.rst:58
msgid ""
"Key draft --> assign them into Time sheet (or adjust them on Timeline, then "
"assign them into Time sheet) --> adjust them on Timeline --> add frames to "
"draw drafts for inbetweening if you need them --> Start drawing Keys"
msgstr ""
"Nyckelutkast --> lägg till dem på tidsblad (eller justera dem på tidslinjen, "
"och tilldela dem därefter till tidsblad) --> justera dem på tidslinjen --> "
"lägg till bildrutor för att rita utkast för mellanteckning om de behövs --> "
"börja rita nyckelbilder"

#: ../../user_manual/japanese_animation_template.rst:61
msgid ".. image:: images/animation/Keys_drafts.png"
msgstr ".. image:: images/animation/Keys_drafts.png"

#: ../../user_manual/japanese_animation_template.rst:62
msgid "You can add layers and add them to timeline."
msgstr "Man kan lägga till lager och placera dem i tidslinjen."

#: ../../user_manual/japanese_animation_template.rst:65
msgid ".. image:: images/animation/Add_Timeline_1.png"
msgstr ".. image:: images/animation/Add_Timeline_1.png"

#: ../../user_manual/japanese_animation_template.rst:67
msgid ".. image:: images/animation/Add_Timeline_2.png"
msgstr ".. image:: images/animation/Add_Timeline_2.png"

#: ../../user_manual/japanese_animation_template.rst:68
msgid ""
"This is due difference between 24 drawing per second, which is used in Full "
"Animation, and 12 drawing per second and 8 drawings per second, which are "
"used in Limited Animation, on the Timeline docker."
msgstr ""
"Det beror på skillnaden mellan 24 bilder per sekund, vilket används i full "
"animering, och 12 bilder per sekund eller 8 bilder per sekund, vilka används "
"i begränsad animering, på tidslinjepanelen."

#: ../../user_manual/japanese_animation_template.rst:71
msgid ".. image:: images/animation/24_12_and_8_drawing_per_sec.png"
msgstr ".. image:: images/animation/24_12_and_8_drawing_per_sec.png"

#: ../../user_manual/japanese_animation_template.rst:72
msgid ""
"This is correspondence between Timeline and Time sheet. \"Black\" layer is "
"to draw main line art which are used ordinary line art, \"Red\" layer is to "
"draw red auxiliary linearts which are used to specify highlights, \"Blue\" "
"layer is to draw blue auxiliary linearts which are used to specify shadows, "
"and \"Shadow\" layer is to draw light green auxiliary line art which are "
"used to specify darker shadows. However, probably you have to increase or "
"decrease these layers according to your work."
msgstr ""
"Det är korrespondensen mellan tidslinje och tidsblad. Det \"svarta\" lagret "
"är till för att rita huvudteckningar, som använder vanlig teckning. Det "
"\"röda\" lagret är till för att rita röda extrateckningar, som används för "
"att ange dagrar. Det \"blåa\" lagret är till för att rita blåa "
"extrateckningar, som används för att ange skuggor. Lagret \"skugga\" används "
"för att rita ljusgröna extrateckningar, som används för att ange mörkare "
"skuggor. Dock måste man troligen öka eller minska dessa lager i enlighet med "
"arbetet."

#: ../../user_manual/japanese_animation_template.rst:75
msgid ".. image:: images/animation/Time_sheet_1.png"
msgstr ".. image:: images/animation/Time_sheet_1.png"

#: ../../user_manual/japanese_animation_template.rst:76
msgid ""
"Finished keys, you will begin to draw the inbetweens. If you feel Krita is "
"becoming slow, I recommend you to merge key drafts and keys, as well as to "
"remove any unnecessary layers."
msgstr ""
"När nyckelbilder är färdiga, börjar man rita mellanteckningarna. Om man "
"märker att Krita börjar bli långsamt, rekommenderar jag att sammanfoga "
"nyckelutkast och nyckelbilder, samt ta bort eventuella onödiga lager."

#: ../../user_manual/japanese_animation_template.rst:78
msgid ""
"After finalizing keys and cleaning up unnecessary layers, add inbetweenings, "
"using Time sheet and inbetweening drafts as reference."
msgstr ""
"Efter att nyckelbilder har slutförts och onödiga lager har städats bort, "
"lägg till mellanteckningar, med användning av tidsblad och utkast till "
"mellanteckningar som referens."

#: ../../user_manual/japanese_animation_template.rst:81
msgid "This is its correspondence with Time sheet."
msgstr "Det är dess korrespondens till tidsblad."

#: ../../user_manual/japanese_animation_template.rst:84
msgid ".. image:: images/animation/Inbetweening.png"
msgstr ".. image:: images/animation/Inbetweening.png"

#: ../../user_manual/japanese_animation_template.rst:85
msgid ""
"Once the vector functionality of Krita becomes better, I recommend you to "
"use vector to finalize inbetweening."
msgstr ""
"När väl vektorfunktionerna i Krita blir bättre, rekommenderar jag att "
"använda vektorer för att färdigställa mellanteckning."

#: ../../user_manual/japanese_animation_template.rst:87
msgid ""
"If you do the colors in Krita, please use Coloring group layer. If you do "
"colors in other software, I recommend to export frames as .TGA files."
msgstr ""
"Om färgerna görs i Krita, använda grupplagret Coloring. Om färgerna görs i "
"en annan programvara, rekommenderar jag att exportera bildrutor som .TGA-"
"filer."

#: ../../user_manual/japanese_animation_template.rst:91
msgid "Resolution"
msgstr "Upplösning"

#: ../../user_manual/japanese_animation_template.rst:93
msgid ""
"I made this template in 300 dpi because we have to print them to use them in "
"traditional works which still fill an important role in Japanese Anime "
"Studio. However, if you stick to digital, 150-120 dpi is enough to make "
"animation. So you can decrease its resolution according to your need."
msgstr ""
"Jag har gjort mallen med 300 punkter/tum eftersom vi måste skriva ut dem för "
"att använda dem i traditionella alster som ännu fyller en viktig roll i "
"japansk Anime Studio. Om man dock håller sig till digitalt är 120 - 150 "
"punkter/tum nog för animeringar, så man kan minska upplösningen efter behov."

#: ../../user_manual/japanese_animation_template.rst:95
msgid ""
"Originally written by Saisho Kazuki, Japanese professional animator, and "
"translated by Tokiedian, KDE contributor."
msgstr ""
"Ursprungligen skrivet av Saisho Kazuki, japansk professionell animatör, och "
"översatt av Tokiedian, KDE-bidragsgivare."
