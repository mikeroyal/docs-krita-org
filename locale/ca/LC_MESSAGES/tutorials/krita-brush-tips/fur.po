# Translation of docs_krita_org_tutorials___krita-brush-tips___fur.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: tutorials\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-08-30 16:59+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.11.70\n"

#: ../../tutorials/krita-brush-tips/fur.rst:None
msgid ""
".. image:: images/brush-tips/Krita-brushtips-fur_01.png\n"
"   :alt: Some example of furs and hair"
msgstr ""
".. image:: images/brush-tips/Krita-brushtips-fur_01.png\n"
"   :alt: Algun exemple de pells i cabell."

#: ../../tutorials/krita-brush-tips/fur.rst:None
msgid ""
".. image:: images/brush-tips/Krita-brushtips-fur_02.png\n"
"   :alt: brush setting dialog for fur brush"
msgstr ""
".. image:: images/brush-tips/Krita-brushtips-fur_02.png\n"
"   :alt: Diàleg de configuració del pinzell per al pinzell de pells."

#: ../../tutorials/krita-brush-tips/fur.rst:None
msgid ""
".. image:: images/brush-tips/Krita-brushtips-fur_03.png\n"
"   :alt: brush setting dialog for fur"
msgstr ""
".. image:: images/brush-tips/Krita-brushtips-fur_03.png\n"
"   :alt: Diàleg de configuració del pinzell per a les pells."

#: ../../tutorials/krita-brush-tips/fur.rst:None
msgid ""
".. image:: images/brush-tips/Krita-brushtips-fur_04.png\n"
"   :alt: brush setting dialog showing color gradation"
msgstr ""
".. image:: images/brush-tips/Krita-brushtips-fur_04.png\n"
"   :alt: Diàleg de configuració mostrant el degradat del color."

#: ../../tutorials/krita-brush-tips/fur.rst:None
msgid ""
".. image:: images/brush-tips/Krita-brushtips-fur_05.png\n"
"   :alt: result of the brush that we made"
msgstr ""
".. image:: images/brush-tips/Krita-brushtips-fur_05.png\n"
"   :alt: El resultat del pinzell que hem creat."

#: ../../tutorials/krita-brush-tips/fur.rst:None
msgid ""
".. image:: images/brush-tips/Krita-brushtips-fur_06.png\n"
"   :alt: using the fur brush to make grass and hair"
msgstr ""
".. image:: images/brush-tips/Krita-brushtips-fur_06.png\n"
"   :alt: Utilitzant el pinzell de pells per a crear herba i pèl."

#: ../../tutorials/krita-brush-tips/fur.rst:None
msgid ""
".. image:: images/brush-tips/Krita-brushtips-fur_07.png\n"
"   :alt: fur brush with the color source to gradient and mix option"
msgstr ""
".. image:: images/brush-tips/Krita-brushtips-fur_07.png\n"
"   :alt: Pinzell de pells amb l'origen del color per al degradat i l'opció "
"de mescla."

#: ../../tutorials/krita-brush-tips/fur.rst:1
msgid "A tutorial about creating fur in Krita"
msgstr "Una guia d'aprenentatge sobre com crear pells en el Krita"

#: ../../tutorials/krita-brush-tips/fur.rst:13
msgid "Brush-tips:Fur"
msgstr "Puntes de pinzell: pells"

#: ../../tutorials/krita-brush-tips/fur.rst:16
msgid "Question"
msgstr "Pregunta"

#: ../../tutorials/krita-brush-tips/fur.rst:18
msgid "What brushes are best for fur textures?"
msgstr "Quins pinzells seran els millors per a les textures de les pells?"

#: ../../tutorials/krita-brush-tips/fur.rst:23
msgid ""
"So typically, you see the same logic applied on fur as on regular :ref:"
"`hair`."
msgstr ""
"En general, veureu la mateixa lògica aplicada a les pells que en els :ref:"
"`hair`."

#: ../../tutorials/krita-brush-tips/fur.rst:25
msgid ""
"However, you can make a brush a little easier by using the Gradient, Mix and "
"HSV options in the :ref:`pixel and color smudge brushes "
"<pixel_brush_engine>`. Basically, what we want to do is have a stroke start "
"dark and then become lighter as we draw with it, to simulate how hair-tips "
"catch more light and look lighter due to being thinner at the ends, while at "
"the base they are frequently more dark."
msgstr ""
"No obstant això, podreu crear un pinzell que sigui una mica més fàcil "
"utilitzant les opcions Degradat, Mescla i HSV en els :ref:`pinzells de "
"píxels i amb esborronat del color <pixel_brush_engine>`. Bàsicament, el que "
"volem fer és que el traç comenci a enfosquir-se i que després es torni més "
"clar a mesura que dibuixem amb ell, per a simular com les puntes dels pels "
"captaran més llum i es veuran més clars pel fet que són més prims en els "
"extrems, mentre que a la base sovint seran més foscos."

#: ../../tutorials/krita-brush-tips/fur.rst:30
msgid ""
"Take the :guilabel:`ink_brush_25` and choose under :menuselection:`Brush Tip "
"--> Predefined “A-2 dirty brush”`. Set the spacing to :guilabel:`Auto` and "
"right-click the spacing bar to type in a value between 0.25 and 0.8. Also "
"turn on the :guilabel:`Enable Pen Settings` on flow. Replicate the pressure "
"curve above on the size option. We don’t want the hairs to collapse to a "
"point, hence why the curve starts so high."
msgstr ""
"Prengui el :guilabel:`Ink_brush_25` i escolliu sota :menuselection:`Punta "
"del pinzell --> Predefinit «A-2 dirty brush». Establiu l'espaiat a :guilabel:"
"`Automàtic` i feu clic dret sobre la barra d'espaiat per escriure un valor "
"entre 0,25 i 0,8. Activeu també :guilabel:`Activa els ajustaments per al "
"llapis` sobre el flux. Repliqueu la corba de pressió anterior a l'opció "
"Mida. No volem que els pèls es col·lapsin en un punt, per això la corba "
"comença tan alta."

#: ../../tutorials/krita-brush-tips/fur.rst:35
msgid ""
"Then activate value and reproduce this curve with the :guilabel:`Distance` "
"or :guilabel:`Fade` sensor. Like how the pressure sensor changes a value "
"(like size) with the amount of pressure you put on the stylus, the distance "
"sensor measures how many pixels your stroke is, and can change an option "
"depending on that. For the HSV sensors: If the curve goes beneath the "
"middle, it’ll become remove from that adjustment, and above the vertical "
"middle it’ll add to that adjustment. So in this case, for the first 100px "
"the brush dab will go from a darkened version of the active paint color, to "
"the active paint color, and then for 100px+ it’ll go from the active color "
"to a lightened version. The curve is an inverse S-curve, because we want to "
"give a lot of room to the mid-tones."
msgstr ""
"Després activeu el valor i reproduïu aquesta corba amb el sensor :guilabel:"
"`Distància` o :guilabel:`Esvaniment`. Igual com el sensor Pressió canvia un "
"valor (com la mida) amb la quantitat de pressió que apliqueu al llapis, el "
"sensor Distància mesura quants píxels té el vostre traç i pot canviar una "
"opció depenent d'això. Per als sensors HSV: si la corba va per sota del mig, "
"s'eliminarà d'aquest ajust, i per sobre del mig vertical s'afegirà a aquest "
"ajust. De manera que en aquest cas, per als primers 100 px, el toc del "
"pinzell passarà d'una versió fosca del color de pintura actiu, al color de "
"pintura actiu, i després per a més de 100 px, passarà del color actiu a una "
"versió més clara. La corba és una corba S inversa, perquè volem donar molt "
"espai als tons mitjans."

#: ../../tutorials/krita-brush-tips/fur.rst:40
msgid ""
"We do the same thing for saturation, so that the darkened color is also "
"slightly desaturated. Notice how the curve is close to the middle: This "
"means its effect is much less strong than the value adjustment. The result "
"should look somewhat like the fifth one from the left on the first row of "
"this:"
msgstr ""
"Farem el mateix per a la saturació, de manera que el color fosc també "
"estigui lleugerament dessaturat. Observeu com la corba es troba a prop del "
"mig: això vol dir que el seu efecte serà molt menys fort que l'ajust del "
"valor. El resultat hauria de semblar-se al cinquè de l'esquerra a la primera "
"fila:"

#: ../../tutorials/krita-brush-tips/fur.rst:45
msgid ""
"The others are done with the smudge brush engine, but a similar setup, "
"though using color rate on distance instead. Do note that it’s very hard to "
"shade realistic fur, so keep a good eye on your form shadow. You can also "
"use this with grass, feathers and other vegetation:"
msgstr ""
"Els altres es realitzen amb el motor de pinzell amb esborronat, però amb una "
"configuració similar, encara que en el seu lloc s'utilitza la taxa de color "
"sobre la distància. Recordeu que és molt difícil ombrejar un pèl realista, "
"així que vigileu bé la forma de la vostra ombra. També podreu utilitzar-ho "
"amb l'herba, plomes i altra vegetació:"

#: ../../tutorials/krita-brush-tips/fur.rst:50
msgid ""
"For example, if you use the mix option in the pixel brush, it’ll mix between "
"the fore and background color. You can even attach a gradient to the color "
"smudge brush and the pixel brush. For color smudge, this is just the :"
"guilabel:`Gradient` option, and it’ll use the active gradient. For the pixel "
"brush, set the color-source to :guilabel:`Gradient` and use the mix option."
msgstr ""
"Per exemple, si utilitzeu l'opció de mescla en el pinzell de píxels, es "
"mesclarà entre el color frontal i el de fons. Fins i tot podreu adjuntar un "
"degradat al pinzell amb esborronat del color i al pinzell de píxels. Per a "
"l'esborronat del color, aquesta és simplement l'opció :guilabel:`Degradat`, "
"i utilitzarà el degradat actiu. Per al pinzell de píxels, establiu la font "
"del color a :guilabel:`Degradat` i utilitzeu l'opció de mescla."

# skip-rule: t-acc_obe
#: ../../tutorials/krita-brush-tips/fur.rst:55
msgid ""
"On tumblr it was suggested this could be used to do `this tutorial <https://"
"vimeo.com/78183651>`_. Basically, you can also combine this with the lighter "
"color blending mode and wraparound mode to make making grass-textures really "
"easy!"
msgstr ""
"A Tumblr es va suggerir que això podria emprar-se per a fer `aquesta guia "
"d'aprenentatge <https://vimeo.com/78183651>`_. Bàsicament, també podreu "
"combinar-ho amb el mode de barreja Colors més clars i el mode Ajust cíclic "
"per a fer que les textures de l'herba siguin realment fàcils."
