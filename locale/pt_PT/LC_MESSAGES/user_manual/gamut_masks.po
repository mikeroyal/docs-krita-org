# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.2\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-06 03:34+0200\n"
"PO-Revision-Date: 2019-05-21 18:46+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: Gamute dockers vectorgraphics Krita menuselection\n"
"X-POFile-SpellExtra: image resourcemanagement YouTube Gourney\n"
"X-POFile-SpellExtra: advancedcolorselectordocker images\n"
"X-POFile-SpellExtra: artisticcolorselectordocker gamutmaskdocker guilabel\n"
"X-POFile-SpellExtra: Gurney KritaGamutMaskDocker ref GamutMasksSelectors\n"

#: ../../user_manual/gamut_masks.rst:1
msgid "Basics of using gamut masks in Krita."
msgstr "Bases da utilização de máscaras de gamute no Krita."

#: ../../user_manual/gamut_masks.rst:10 ../../user_manual/gamut_masks.rst:15
msgid "Gamut Masks"
msgstr "Máscaras do Gamute"

#: ../../user_manual/gamut_masks.rst:19
msgid ""
"Gamut masking is an approach to color formalized by James Gurney, based on "
"the idea that any color scheme can be expressed as shapes cut out from the "
"color wheel."
msgstr ""
"As máscaras do gamute são uma abordagem à cor formalizada por James Gurney, "
"com base na ideia de que qualquer esquema de cores pode ser expresso como "
"formas recortadas na roda de cores."

#: ../../user_manual/gamut_masks.rst:21
msgid ""
"It originates in the world of traditional painting, as a form of planning "
"and premixing palettes. However, it translates well into digital art, "
"enabling you to explore and analyze color, plan color schemes and guide your "
"color choices."
msgstr ""
"Tem origem no mundo da pintura tradicional, como uma forma de planear e "
"misturar previamente as paletas. Contudo, traduz-se bem na arte digital, "
"permitindo-lhe explorar e analisar a cor, planear os esquemas de cores e "
"guiá-lo nas suas escolhas de cores."

#: ../../user_manual/gamut_masks.rst:23
msgid "How does it work?"
msgstr "Como é que funciona?"

#: ../../user_manual/gamut_masks.rst:25
msgid ""
"You draw one or multiple shapes on top of the color wheel. You limit your "
"color choices to colors inside the shapes. By leaving colors out, you "
"establish a relationship between the colors, thus creating harmony."
msgstr ""
"Você desenha uma ou mais formas sobre a roda de cores. Irá limitar as suas "
"escolhas de cores às que constam dentro das formas. Ao deixar cores de fora, "
"irá estabelecer uma relação entre as cores, criando assim alguma harmonia."

#: ../../user_manual/gamut_masks.rst:28
msgid ""
"Gamut masking is available in both the Advanced and Artistic Color Selectors."
msgstr ""
"A máscara do gamute está disponível tanto nos Selectores de Cores Avançados "
"como nos Artísticos."

#: ../../user_manual/gamut_masks.rst:33
msgid ""
"`Color Wheel Masking, Part 1 by James Gurney <https://gurneyjourney.blogspot."
"com/2008/01/color-wheel-masking-part-1.html>`_"
msgstr ""
"Máscara da Roda de Cores, Parte 1 de James Gurney <https://gurneyjourney."
"blogspot.com/2008/01/color-wheel-masking-part-1.html>`_"

#: ../../user_manual/gamut_masks.rst:34
msgid ""
"`The Shapes of Color Schemes by James Gurney <https://gurneyjourney.blogspot."
"com/2008/02/shapes-of-color-schemes.html>`_"
msgstr ""
"`As Formas dos Esquemas de Cores de James Gurney <https://gurneyjourney."
"blogspot.com/2008/02/shapes-of-color-schemes.html>`_"

#: ../../user_manual/gamut_masks.rst:35
msgid ""
"`Gamut Masking Demonstration by James Gourney (YouTube) <https://youtu.be/"
"qfE4E5goEIc>`_"
msgstr ""
"`Demonstração da Máscara do Gamute de James Gourney (YouTube) <https://youtu."
"be/qfE4E5goEIc>`_"

#: ../../user_manual/gamut_masks.rst:39
msgid "Selecting a gamut mask"
msgstr "Seleccionar uma máscara de gamute"

#: ../../user_manual/gamut_masks.rst:41
msgid ""
"For selecting and managing gamut masks open the :ref:`gamut_mask_docker`:  :"
"menuselection:`Settings --> Dockers --> Gamut Masks`."
msgstr ""
"Para seleccionar e gerir as máscaras do gamute, abra a :ref:"
"`gamut_mask_docker`:  :menuselection:`Configuração --> Áreas Acopláveis --> "
"Máscaras do Gamute`."

#: ../../user_manual/gamut_masks.rst:45
msgid ".. image:: images/dockers/Krita_Gamut_Mask_Docker.png"
msgstr ".. image:: images/dockers/Krita_Gamut_Mask_Docker.png"

#: ../../user_manual/gamut_masks.rst:45
msgid "Gamut Masks docker"
msgstr "Área de Máscaras do Gamute"

#: ../../user_manual/gamut_masks.rst:47
msgid ""
"In this docker you can choose from several classic gamut masks, like the "
"‘Atmospheric Triad’, ‘Complementary’, or ‘Dominant Hue With Accent’. You can "
"also duplicate those masks and make changes to them (3,4), or create new "
"masks from scratch (2)."
msgstr ""
"Nesta área, poderá escolher entre várias máscaras de gamute clássicas, como "
"a ‘Tríade Atmosférica’, a ‘Complementar’ ou o ‘Tom Dominante Acentuado’. "
"Poderá também duplicar essas máscaras e fazer alterações às mesmas (3,4), ou "
"ainda criar máscaras novas do zero (2)."

#: ../../user_manual/gamut_masks.rst:49
msgid ""
"Clicking the thumbnail icon (1) of the mask applies it to the color "
"selectors."
msgstr ""
"Se carregar no ícone da miniatura (1) da máscara, aplicá-lo-á aos selectores "
"de cores."

#: ../../user_manual/gamut_masks.rst:53
msgid ":ref:`gamut_mask_docker`"
msgstr ":ref:`gamut_mask_docker`"

#: ../../user_manual/gamut_masks.rst:57
msgid "In the color selector"
msgstr "No selector de cores"

#: ../../user_manual/gamut_masks.rst:59
msgid ""
"You can rotate an existing mask directly in the color selector, by dragging "
"the rotation slider on top of the selector (2)."
msgstr ""
"Poderá rodar uma máscara existente no selector de cores, arrastando a barra "
"de rotação no topo do selector (2)."

#: ../../user_manual/gamut_masks.rst:61
msgid ""
"The mask can be toggled off and on again by the toggle mask button in the "
"top left corner (1)."
msgstr ""
"A máscara poderá ser activada e desactivada, usando o botão de comutação da "
"máscara no canto superior esquerdo (1)."

#: ../../user_manual/gamut_masks.rst:65
msgid ".. image:: images/dockers/GamutMasks_Selectors.png"
msgstr ".. image:: images/dockers/GamutMasks_Selectors.png"

#: ../../user_manual/gamut_masks.rst:65
msgid "Advanced and Artistic color selectors with a gamut mask"
msgstr "Selectores de cores Avançado e Artístico com uma máscara de gamute"

#: ../../user_manual/gamut_masks.rst:69
msgid ":ref:`artistic_color_selector_docker`"
msgstr ":ref:`artistic_color_selector_docker`"

#: ../../user_manual/gamut_masks.rst:70
msgid ":ref:`advanced_color_selector_docker`"
msgstr ":ref:`advanced_color_selector_docker`"

#: ../../user_manual/gamut_masks.rst:74
msgid "Editing/creating a custom gamut mask"
msgstr "Editar/criar uma máscara de gamute personalizada"

#: ../../user_manual/gamut_masks.rst:78
msgid ""
"To rotate a mask around the center point use the rotation slider in the "
"color selector."
msgstr ""
"Para rodar uma máscara em torno do centro, use a barra de rotação no "
"selector de cores."

#: ../../user_manual/gamut_masks.rst:80
msgid ""
"If you choose to create a new mask, edit, or duplicate selected mask, the "
"mask template documents open as a new view (1)."
msgstr ""
"Se optar por criar uma nova máscara, editar ou duplicar uma existente, os "
"documentos de modelos de máscara aparecem numa nova área (1)."

#: ../../user_manual/gamut_masks.rst:82
msgid ""
"There you can create new shapes and modify the mask with standard vector "
"tools (:ref:`vector_graphics`). Please note, that the mask is intended to be "
"composed of basic vector shapes. Although interesting results might arise "
"from using advanced vector drawing techniques, not all features are "
"guaranteed to work properly (e.g. grouping, vector text, etc.)."
msgstr ""
"Aí poderá criar novas formas e modificar a máscara com ferramentas "
"vectoriais normais (:ref:`vector_graphics`). Lembre-se contudo que a máscara "
"pretende ser comporta por formas vectoriais básicas. Ainda que possam surgir "
"resultados interessantes da utilização de técnicas de desenho vectorial "
"avançadas, nem todas as funcionalidades têm a garantia de funcionar (p.ex., "
"agrupamentos, texto vectorial, etc.)."

#: ../../user_manual/gamut_masks.rst:86
msgid ""
"Transformations done through the transform tool or layer transform cannot be "
"saved in a gamut mask. The thumbnail image reflects the changes, but the "
"individual mask shapes do not."
msgstr ""
"As transformações efectuadas na ferramenta de transformação ou na "
"transformação de camadas não poderão ser gravadas numa máscara do gamute. A "
"imagem em miniatura reflecte as alterações, mas as formas individuais não."

#: ../../user_manual/gamut_masks.rst:88
msgid ""
"You can :guilabel:`Preview` the mask in the color selector (4). If you are "
"satisfied with your changes, :guilabel:`Save` the mask (5). :guilabel:"
"`Cancel` (3) will close the editing view without saving your changes."
msgstr ""
"Poderá :guilabel:`Antever` a máscara  no selector de cores (4). Se estiver "
"satisfeito com as suas alterações, poderá :guilabel:`Gravar` a máscara (5). "
"Se :guilabel:`Cancelar` (3), irá fechar a área de edição sem gravar as suas "
"alterações."

#: ../../user_manual/gamut_masks.rst:92
msgid ".. image:: images/dockers/Krita_Gamut_Mask_Docker_2.png"
msgstr ".. image:: images/dockers/Krita_Gamut_Mask_Docker_2.png"

#: ../../user_manual/gamut_masks.rst:92
msgid "Editing a gamut mask"
msgstr "Editar uma máscara do gamute"

#: ../../user_manual/gamut_masks.rst:96
msgid "Importing and exporting"
msgstr "Importar e exportar"

#: ../../user_manual/gamut_masks.rst:98
msgid ""
"Gamut masks can be imported and exported in bundles in the Resource Manager. "
"See :ref:`resource_management` for more information."
msgstr ""
"As máscaras do gamute podem ser importadas e exportadas em pacotes no Gestor "
"de Recursos. Veja mais informações em :ref:`resource_management`."
