# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-18 13:04+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: toolfill icons Krita filltool image images alt\n"

#: ../../<generated>:1
msgid "Use Pattern"
msgstr "Usar um Padrão"

#: ../../<rst_epilog>:56
msgid ""
".. image:: images/icons/fill_tool.svg\n"
"   :alt: toolfill"
msgstr ""
".. image:: images/icons/fill_tool.svg\n"
"   :alt: ferramenta de preenchimento"

#: ../../reference_manual/tools/fill.rst:1
msgid "Krita's fill tool reference."
msgstr "A referência da ferramenta de preenchimento do Krita."

#: ../../reference_manual/tools/fill.rst:11
msgid "Tools"
msgstr "Ferramentas"

#: ../../reference_manual/tools/fill.rst:11
msgid "Fill"
msgstr "Preenchimento"

#: ../../reference_manual/tools/fill.rst:11
msgid "Bucket"
msgstr "Grupo"

#: ../../reference_manual/tools/fill.rst:16
msgid "Fill Tool"
msgstr "Ferramenta de Preenchimento"

#: ../../reference_manual/tools/fill.rst:18
msgid "|toolfill|"
msgstr "|toolfill|"

#: ../../reference_manual/tools/fill.rst:20
msgid ""
"Krita has one of the most powerful and capable Fill functions available. The "
"options found in the Tool Options docker and outlined below will give you a "
"great deal of flexibility working with layers and selections."
msgstr ""
"O Krita tem uma das funcionalidades de preenchimento mais poderosas e aptas "
"disponíveis. As opções disponíveis na área de Opções da Ferramenta e "
"explicadas abaixo dar-lhe-ão uma grande dose de flexibilidade ao lidar com "
"as camadas e selecções."

#: ../../reference_manual/tools/fill.rst:22
msgid ""
"To get started, clicking anywhere on screen with the fill-tool will allow "
"that area to be filed with the foreground color."
msgstr ""
"Para começar, se carregar em qualquer ponto do ecrã com a ferramenta de "
"preenchimento, permitirá que essa área seja preenchida com a cor principal."

#: ../../reference_manual/tools/fill.rst:25
msgid "Tool Options"
msgstr "Opções da Ferramenta"

#: ../../reference_manual/tools/fill.rst:27
msgid "Fast Mode"
msgstr "Modo Rápido"

#: ../../reference_manual/tools/fill.rst:28
msgid ""
"This is a special mode for really fast filling. However, many functions "
"don't work with this mode."
msgstr ""
"Este é um modo especial para um preenchimento realmente rápido. Contudo, "
"muitas das funções não funcionam com este modo."

#: ../../reference_manual/tools/fill.rst:29
msgid "Threshold"
msgstr "Limiar"

#: ../../reference_manual/tools/fill.rst:30
msgid "Determines when the fill-tool sees another color as a border."
msgstr ""
"Define quando é que a ferramenta de preenchimento vê outra cor como sendo um "
"contorno."

#: ../../reference_manual/tools/fill.rst:31
msgid "Grow Selection"
msgstr "Aumentar a Selecção"

#: ../../reference_manual/tools/fill.rst:32
msgid "This value extends the shape beyond its initial size."
msgstr "Este valor expande a forma para além do seu tamanho inicial."

#: ../../reference_manual/tools/fill.rst:33
msgid "Feathering Radius"
msgstr "Raio do Contorno Suave"

#: ../../reference_manual/tools/fill.rst:34
msgid "This value will add a soft border to the filled-shape."
msgstr "Este valor irá adicionar um contorno suavizado à forma preenchida."

#: ../../reference_manual/tools/fill.rst:35
msgid "Fill Entire Selection"
msgstr "Preencher a Selecção Inteira"

#: ../../reference_manual/tools/fill.rst:36
msgid ""
"Activating this will result in the shape filling the whole of the active "
"selection, regardless of threshold."
msgstr ""
"Se activar isto, fará com que a forma preencha a totalidade da selecção "
"activa, independentemente do limiar definido."

#: ../../reference_manual/tools/fill.rst:37
msgid "Limit to current layer"
msgstr "Limitar à camada actual"

#: ../../reference_manual/tools/fill.rst:38
msgid ""
"Activating this will prevent the fill tool from taking other layers into "
"account."
msgstr ""
"Se activar isto, irá impedir a ferramenta de preenchimento de ter as outras "
"camadas em conta."

#: ../../reference_manual/tools/fill.rst:40
msgid "Ticking this will result in the active pattern being used."
msgstr "Se assinalar isto, fará com que seja usado o padrão activo."
